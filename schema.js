const { buildSchema } = require('graphql');

// eslint-disable-next-line new-cap
module.exports = new buildSchema(`
    scalar Date

    input DogInput {
        _id: String
    }

    type Cat {
        _id: ID
        name: String
        breed: String
        image: String
    }

    type Dog {
        _id: ID
        name: String
        breed: String
        color: String
        image: String
    }

    type Owner {
        _id: ID
        firstname: String
        lastname: String
        dob: Date
        dogs: [Dog]
        cats: [Cat]
    }

    type Query {
        cats: [Cat]
        dogs: [Dog]
        owners: [Owner]
    }

    type Mutation {
        addDogToOwner(ownerId: String!, dogs: [DogInput!]): Owner
        removeDogFromOwner(ownerId: String!, dogId: ID!): Owner
    }
`);

// addDogToOwner(ownerId: String!, dogId: String!): Owner!
// removeDogFromOwner(ownerId: String!, dogId: String!): Owner!
