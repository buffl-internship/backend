const { GraphQLScalarType, Kind } = require('graphql');
const Cat = require('./models/Cat');
const Dog = require('./models/Dog');
const Owner = require('./models/Owner');

const dateScalar = new GraphQLScalarType({
  name: 'Date',
  description: 'Custom Date Scalar Type',
  serialize(value) {
    return value.getTime();
  },
  parseValue(value) {
    return new Date(value);
  },
  parseLiteral({ kind }) {
    if (kind === Kind.INT) {
      return new Date(parseInt(kind.value, 10));
    }
    return null;
  },
});

const resolvers = {
  Date: dateScalar,
  cats: async () => {
    const catsFound = await Cat.find();
    return catsFound;
  },
  dogs: async () => {
    const dogsFound = await Dog.find();
    return dogsFound;
  },
  owners: async () => {
    const ownersFound = await Owner.find().populate('dogs').populate('cats');
    return ownersFound;
  },

  addDogToOwner: async (args) => {
    await Owner.findOneAndUpdate(
      {
        _id: args.ownerId,
      },
      {
        $push: {
          dogs: args.dogs,
        },
      },
      { useFindAndModify: false }
    );
    const owner = await Owner.findOne({ _id: args.ownerId }).populate('dogs');
    return owner;
  },
  removeDogFromOwner: async (args) => {
    await Owner.findOneAndUpdate(
      {
        _id: args.ownerId,
      },
      {
        $pull: {
          dogs: args.dogId,
        },
      },
      { useFindAndModify: false }
    );

    const owner = await Owner.findOne({ _id: args.ownerId }).populate('dogs');
    return owner;
  },
};

module.exports = resolvers;
