const mongoose = require('mongoose');

const { Schema } = mongoose;

const CatSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  breed: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Cat', CatSchema);
