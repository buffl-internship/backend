const mongoose = require('mongoose');

const { Schema } = mongoose;

const OwnerSchema = new Schema({
  firstname: {
    type: String,
    required: true,
  },
  lastname: {
    type: String,
    required: true,
  },
  dob: {
    type: Date,
    required: true,
  },
  dogs: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Dog',
    },
  ],
  cats: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Cat',
    },
  ],
});

module.exports = mongoose.model('Owner', OwnerSchema);
