const mongoose = require('mongoose');

const { Schema } = mongoose;

const DogSchema = new Schema({
  name: {
    type: String,
    required: true,
    default: 'Unnamed',
  },
  breed: {
    type: String,
    required: true,
  },
  color: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Dog', DogSchema);
