require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const { graphqlHTTP } = require('express-graphql');
const cors = require('cors');

// Import GraphQL components
const schema = require('./schema');
const resolvers = require('./resolvers');

// Import routes
const dogsRoute = require('./routes/dogs');
const catsRoute = require('./routes/cats');
const ownersRoute = require('./routes/owners');

const app = express();

app.listen(process.env.DB_PORT);

// Middlewares
app.use(express.json());
app.use(cors());
app.use('/dogs', dogsRoute);
app.use('/cats', catsRoute);
app.use('/owners', ownersRoute);
app.use(
  '/graphql',
  graphqlHTTP({
    schema,
    rootValue: resolvers,
    graphiql: true,
  })
);

// ROUTES
app.get('/', (req, res) => {
  res.send('Test');
});

// Connect to MongoDB
mongoose.connect(
  // MongoDB local
  process.env.DB_PETS_LOCAL,

  // MongoDB Atlas
  // process.env.DB_PETS_URI,
  { useNewUrlParser: true },
  // eslint-disable-next-line no-console
  () => console.log('Connected to MongoDB!')
);
