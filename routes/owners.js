const express = require('express');
const Owner = require('../models/Owner');
const Dog = require('../models/Dog');
const Cat = require('../models/Cat');

const router = express.Router();

// GET: ALL OWNERS
router.get('/', async (req, res) => {
  try {
    const owners = await Owner.find().populate('dogs').populate('cats');
    res.json(owners);
  } catch (err) {
    res.json({ message: err });
  }
});

// GET: SPECIFIC OWNER
router.get('/:ownerId', async (req, res) => {
  try {
    const owner = await Owner.findById(req.params.ownerId)
      .populate('dogs')
      .populate('cats');
    res.json(owner);
  } catch (err) {
    res.json({ message: err });
  }
});

// POST: OWNER
router.post('/', async (req, res) => {
  const owner = new Owner({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    dob: req.body.dob,
  });

  try {
    const savedOwner = await owner.save();
    res.json(savedOwner);
  } catch (err) {
    res.json({ message: err });
  }
});

// DELETE: OWNER
router.delete('/:ownerId', async (req, res) => {
  try {
    const deletedOwner = await Owner.remove(req.params.ownerId);
    res.json(deletedOwner);
  } catch (err) {
    res.json({ message: res });
  }
});

// UPDATE:  OWNER -> DOGS
router.patch('/:ownerId/dogs', async (req, res) => {
  try {
    const dogFound = await Dog.findById(req.body._id);
    const updatedOwner = await Owner.findByIdAndUpdate(req.params.ownerId, {
      $push: {
        dogs: dogFound,
      },
    });
    res.json(updatedOwner);
  } catch (err) {
    res.json({ message: err });
  }
});

// UPDATE:  OWNER -> CATS
router.patch('/:ownerId/cats', async (req, res) => {
  try {
    const catFound = await Cat.findById(req.body._id);
    const updatedOwner = await Owner.findByIdAndUpdate(req.params.ownerId, {
      $push: {
        cats: catFound,
      },
    });
    res.json(updatedOwner);
  } catch (err) {
    res.json({ message: err });
  }
});

// DELETE CAT FROM OWNER's DOG ARRAY
router.delete('/:ownerId/dogs/:dogId', async (req, res) => {
  try {
    const dogFound = await Dog.findById(req.params.dogId);
    const deletedDog = await Owner.updateOne(
      { _id: req.params.ownerId },
      {
        $pull: {
          dogs: dogFound._id,
        },
      }
    );
    res.json(deletedDog);
  } catch (err) {
    res.json({ message: err });
  }
});

// DELETE CAT FROM OWNER's CAT ARRAY
router.delete('/:ownerId/cats/:catId', async (req, res) => {
  try {
    const catFound = await Cat.findById(req.params.catId);
    const deletedCat = await Owner.updateOne(
      { _id: req.params.ownerId },
      {
        $pull: {
          cats: catFound,
        },
      }
    );
    res.json(deletedCat);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
