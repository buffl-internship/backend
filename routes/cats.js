const express = require('express');
const Cat = require('../models/Cat');

const router = express.Router();

// GET: ALL CATS
router.get('/', async (req, res) => {
  try {
    const cats = await Cat.find();
    res.json(cats);
  } catch (err) {
    res.json({ message: err });
  }
});

// GET: SPECIFIC CAT
router.get('/:catId', async (req, res) => {
  try {
    const cat = await Cat.findById(req.params.catId);
    res.json(cat);
  } catch (err) {
    res.json({ message: err });
  }
});

// POST: CAT
router.post('/', async (req, res) => {
  const cat = new Cat({
    name: req.body.name,
    breed: req.body.breed,
    image: req.body.image,
  });

  try {
    const savedCat = await cat.save();
    res.json(savedCat);
  } catch (err) {
    res.json({ message: err });
  }
});

// DELETE: SPECIFIC CAT
router.delete('/:catId', async (req, res) => {
  try {
    const removedCat = await Cat.remove({ _id: req.params.catId });
    res.json(removedCat);
  } catch (err) {
    res.json({ message: err });
  }
});

// UPDATE: SPECIFIC CAT -> COLOR
router.patch('/:catId', async (req, res) => {
  try {
    const updatedCat = await Cat.updateOne(
      { _id: req.params.catId },
      { $set: { name: req.body.name } }
    );
    res.json(updatedCat);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
