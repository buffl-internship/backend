const express = require('express');
const Dog = require('../models/Dog');

const router = express.Router();

// GET: ALL DOGS
router.get('/', async (req, res) => {
  try {
    const dogs = await Dog.find();
    res.json(dogs);
  } catch (err) {
    res.json({ message: err });
  }
});

// GET: SPECIFIC DOG
router.get('/:dogId', async (req, res) => {
  try {
    const dog = await Dog.findById(req.params.dogId);
    res.json(dog);
  } catch (err) {
    res.json({ message: err });
  }
});

// POST: DOG
router.post('/', async (req, res) => {
  const dog = new Dog({
    name: req.body.name,
    breed: req.body.breed,
    color: req.body.color,
    image: req.body.image,
  });

  try {
    const savedDog = await dog.save();
    res.json(savedDog);
  } catch (err) {
    res.json({ message: err });
  }
});

// DELETE: SPECIFIC DOG
router.delete('/:dogId', async (req, res) => {
  try {
    const removedDog = await Dog.remove({ _id: req.params.dogId });
    res.json(removedDog);
  } catch (err) {
    res.json({ message: err });
  }
});

// UPDATE: SPECIFIC DOG -> COLOR
router.patch('/:dogId', async (req, res) => {
  try {
    const updatedDog = await Dog.updateOne(
      { _id: req.params.dogId },
      { $set: { color: req.body.color } }
    );
    res.json(updatedDog);
  } catch (err) {
    res.json({ message: err });
  }
});

// // UPDATE: ASSIGN AN OWNER TO SPECIFIC DOG
// router.patch('/:dogID', async (req, res) => {

// });

module.exports = router;
